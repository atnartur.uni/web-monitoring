# web-monitoring

## Запуск для локальной разработки

1. `python3 -m venv env` - инициализация виртуального окружения
2. `source env/bin/activate` - вход в виртуальное окружение
3. `pip install -r requirements.txt` - установка зависимостей
4. `docker-compose up` - поднятие базы данных (если установлен Docker)
5. `python src/manage.py migrate` - запуск миграций
6. `python src/manage.py runserver` - запуск приложения
7. `python src/manage.py createsuperuser` - создание суперпользователя
8. `celery -A webmonitoring worker -l INFO -Q default -B` - запуск celery worker
9. `pre-commit install` - установка pre-commit hooks

## Запуск проекта в production

- `docker-compose -f docker-compose.prod.yml build` - сборка образов
- `docker-compose -f docker-compose.prod.yml up -d` - поднятие контейнеров
- `docker-compose -f docker-compose.prod.yml stop` - остановка контейнеров
- `docker-compose -f docker-compose.prod.yml rm -f` - удаление контейнеров
- `docker exec -ti web-monitoring_app_1 bash` - вход в python-контейнер
- `docker exec -ti web-monitoring_app_1 python src/manage.py createsuperuser` - создание суперпользователя
