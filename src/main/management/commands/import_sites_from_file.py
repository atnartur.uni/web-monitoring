import sys

from django.core.management import BaseCommand

from main.models import Site, User


class Command(BaseCommand):
    help = "Imports list of sites to the system"

    def add_arguments(self, parser):
        parser.add_argument("--file", dest="file", help="Path to file (not required)")

    def handle(self, *args, file=None, **options):
        if file is None:
            fd = sys.stdin
        else:
            fd = open(file, "r")

        user = User.objects.filter(is_superuser=True).first()
        site_urls = [line.strip() for line in fd]
        added_sites = list(Site.objects.filter(url__in=site_urls).values_list("url", flat=True))

        new_sites = []
        for site in site_urls:
            if site not in added_sites:
                new_sites.append(site)

        new_site_models = [Site(url=site, status=True, user=user) for site in new_sites]
        Site.objects.bulk_create(new_site_models)

        print(len(new_site_models), "added")
