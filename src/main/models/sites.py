from django.db import models
from django.db.models import Manager, Subquery, OuterRef

from main.models.base import BaseModel
from .user import User


class SiteManager(Manager):
    def annotate_newest_comment_text(self):
        newest_comments = StatusComment.objects.filter(sites__id=OuterRef("pk")).order_by("-created_at")
        return self.annotate(newest_comment_text=Subquery(newest_comments.values("text")[:1]))


class Site(BaseModel):
    objects = SiteManager()

    url = models.URLField()
    status = models.BooleanField(verbose_name="Работает ли сейчас")
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь", related_name="sites")

    def __str__(self):
        return self.url

    def update_status(self):
        last_check = self.checks.last()
        if last_check:
            self.status = last_check.is_success

    def save(self, *args, **kwargs):
        self.update_status()
        super(Site, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "сайт"
        verbose_name_plural = "сайты"


class SiteCheck(BaseModel):
    site = models.ForeignKey(Site, on_delete=models.CASCADE, verbose_name="Сайт", related_name="checks")
    last_url = models.CharField(max_length=500, verbose_name="Последний URL, который открылся после всех редиректов")
    status_code = models.IntegerField()
    error_response_content = models.TextField(null=True, blank=True, verbose_name="Тело ответа при ошибочном запросе")

    @property
    def is_success(self):
        return 200 <= self.status_code < 300

    class Meta:
        verbose_name = "проверка"
        verbose_name_plural = "проверки"


class StatusComment(BaseModel):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор", related_name="site_comments")
    text = models.TextField(verbose_name="Текст")
    sites = models.ManyToManyField(Site, verbose_name="Сайт")
    incident_time = models.DateTimeField(verbose_name="Время возникновения инцидента")
    is_show_on_main_page = models.BooleanField(default=False, verbose_name="Показывать ли на главной странице")

    class Meta:
        verbose_name = "комментарий к сайту"
        verbose_name_plural = "комментарии к сайту"
