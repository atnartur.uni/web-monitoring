async function loadSites() {
    const res = await fetch('/api/sites/');
    return res.json();
}

function renderSites(sites) {
    const sitesHtml = sites.map(
        site => `<a href="/sites/${site.id}" class="list-group-item">
                ${site.domain}
                <span
                    class="badge bg-${site.status ? 'success' : 'danger'} text-white float-right"
                >
                    ${site.status ? 'OK' : 'ERROR'}
                </span>
                <div class="progress">
                    <div class="progress-bar w-50 bg-danger" role="progressbar" aria-valuemin="0" aria-valuemax="50">ERROR</div>
                    <div class="progress-bar w-50 bg-success" role="progressbar" aria-valuemin="50" aria-valuemax="100">UP</div>
                </div>
            </a>`
    );
    return sitesHtml.join("");
}

document.addEventListener('DOMContentLoaded', async function () {
    const sites = await loadSites();
    const html = renderSites(sites.results);
    const sitesElement = document.querySelector('#sites');
    sitesElement.innerHTML = html;
});
