import Vue from 'vue';
import Vuex from 'vuex';
import {loadSites} from "./services/api";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 0,
        sites: [],
        isLoading: false
    },
    mutations: {
        increment(state) {
            state.count++;
        },
        loading(state, isLoading = true) {
            state.isLoading = isLoading;
        },
        sites(state, sites) {
            state.sites = sites;
        }
    },
    actions: {
        makeIncrement({commit}) {
            commit('increment');
        },
        async load({commit}) {
            commit('loading');
            commit('sites', []);
            const responseObject = await loadSites();
            commit('sites', responseObject.results);
            commit('loading', false);
        }
    }
});

export default store;
