import Vue from 'vue'
import Auth from '../containers/Auth.vue'
import store from '../store';

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(Auth),
}).$mount('#auth')
