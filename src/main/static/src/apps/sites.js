import Vue from 'vue'
import Sites from '../containers/Sites.vue'
import store from '../store';

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(Sites),
}).$mount('#sites')
