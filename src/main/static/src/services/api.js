export async function loadSites() {
    const res = await fetch('/api/sites/');
    return res.json();
}
