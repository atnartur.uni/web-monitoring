module.exports = {
    filenameHashing: false,
    pages: {
        main: './src/apps/sites.js',
        auth: './src/apps/auth.js'
    }
};
