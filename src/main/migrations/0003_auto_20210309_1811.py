# Generated by Django 3.1.6 on 2021-03-09 15:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("main", "0002_user_role"),
    ]

    operations = [
        migrations.RenameField(
            model_name="statuscomment",
            old_name="site",
            new_name="sites",
        ),
    ]
