from django.contrib.auth import get_user_model, authenticate, login, logout
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page
from django.views.generic import RedirectView, DetailView, ListView

from main.decorators import not_authorized
from main.forms import AuthForm, RegistrationForm
from main.models import Site
from main.services import get_sites_with_checks, get_sites_by_hour, get_overall_status, register_user

User = get_user_model()


@cache_page(15)
def main_page(request):
    user = User.objects.first()
    sites = get_sites_with_checks(user).order_by("-updated_at")
    site_checks = get_sites_by_hour(Site.objects.first())
    response = render(
        request,
        "main/main.html",
        {"sites": sites, "site_checks": site_checks, "overall_status": get_overall_status(user)},
    )
    return response


@not_authorized
def auth_page(request):
    form = AuthForm()
    if request.method == "POST":
        form = AuthForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]

            user = authenticate(request, email=email, password=password)
            if user is None:
                form.add_error("email", "Неправильный логин или пароль")
            else:
                login(request, user)
                return redirect("main")

    return render(request, "main/auth.html", {"form": form})


@not_authorized
def registration_page(request):
    form = AuthForm()
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            user = register_user(email, password)
            login(request, user)
            return redirect("main")

    return render(request, "main/registration.html", {"form": form})


def profile_page(request):
    return render(request, "main/profile.html")


def logout_page(request):
    logout(request)
    return redirect("main")


class SitesListView(ListView):
    model = Site
    template_name = "main/sites.html"
    paginate_by = 5


class SitePageDetailView(DetailView):
    model = Site
    template_name = "main/site.html"


class RedirectToMainView(RedirectView):
    permanent = False
    query_string = True
    pattern_name = "main"


def site_page_via_site_name(request, site_name):
    return HttpResponse(f"site via name: {site_name}")


def site_page_pagination(request, number):
    return HttpResponse(f"site page number: {number}")
