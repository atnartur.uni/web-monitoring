from django.contrib import admin, messages
from django.utils.datetime_safe import datetime

from main.models import Site, StatusComment, SiteCheck, User


class SiteChecksInline(admin.TabularInline):
    model = SiteCheck
    readonly_fields = ("error_response_content",)
    extra = 1

    def has_add_permission(self, request, obj):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class HttpsFilter(admin.SimpleListFilter):
    title = "https/http"
    parameter_name = "is_https"

    def has_output(self):
        return True

    def lookups(self, request, model_admin):
        return ((True, "https"), (False, "http"))

    def queryset(self, request, queryset):
        qs = queryset
        if self.value():
            if self.value() == str(True):
                qs = qs.filter(url__istartswith="https://")
            if self.value() == str(False):
                qs = qs.filter(url__istartswith="http://")
        return qs


class SiteModelAdmin(admin.ModelAdmin):
    list_display = ("id", "url", "status", "get_user_email", "get_updated_at")
    list_display_links = ("id", "url")
    ordering = ("-updated_at",)
    search_fields = (
        "id",
        "url",
    )
    list_filter = ("status", HttpsFilter, "updated_at")
    readonly_fields = ("status", "get_created_at", "get_updated_at")
    inlines = [SiteChecksInline]
    actions = ["update_dates"]
    change_form_template = "main/admin/site_change.html"

    def get_queryset(self, request):
        qs = super(SiteModelAdmin, self).get_queryset(request)
        return qs.select_related("user")

    def get_created_at(self, instance: Site):
        return instance.created_at.strftime("%d.%m.%Y")

    get_created_at.short_description = "Дата создания"

    def get_updated_at(self, instance: Site):
        return instance.updated_at.strftime("%d.%m.%Y")

    get_updated_at.short_description = "Дата изменения"

    def get_user_email(self, instance: Site):
        return instance.user.email

    get_user_email.short_description = "Почта пользователя"
    get_user_email.admin_order_field = "user__email"

    def update_dates(self, request, queryset):
        models = []
        for model in queryset:
            model.updated_at = datetime.now()
            models.append(model)
        Site.objects.bulk_update(models, ["updated_at"])
        self.message_user(request, "Даты обновлены", messages.SUCCESS)

    update_dates.short_description = "Обновить дату"

    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        is_custom_button_pressed = request.POST.get("custom_button", False)
        print("is_custom_button_pressed", is_custom_button_pressed)
        return super(SiteModelAdmin, self).changeform_view(request, object_id, form_url, extra_context)


admin.site.register(Site, SiteModelAdmin)
admin.site.register(SiteCheck)
admin.site.register(StatusComment)
admin.site.register(User)
