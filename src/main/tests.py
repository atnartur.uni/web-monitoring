import factory
from django.contrib.auth import get_user_model
from django.test import TestCase
from factory.django import DjangoModelFactory

from main.models import Site, SiteCheck
from main.services import get_overall_status

User = get_user_model()


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Faker("email")


class SiteFactory(DjangoModelFactory):
    class Meta:
        model = Site

    url = factory.Faker("url")


class ServicesTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.site = SiteFactory(user=self.user, status=True)

    def test_get_overall_status_false(self):
        SiteCheck.objects.create(site=self.site, status_code=404)
        self.site.save()
        result = get_overall_status(self.user)
        self.assertFalse(result)

    def test_get_overall_status_true(self):
        SiteCheck.objects.create(site=self.site, status_code=200)
        self.site.save()
        result = get_overall_status(self.user)
        self.assertTrue(result)
