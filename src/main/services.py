import requests
from django.contrib.auth import get_user_model
from django.db.models import QuerySet, Count, Q
from django.db.models.functions import TruncHour

from main.models import Site, SiteCheck
from main.models.user import Role

User = get_user_model()


def get_overall_status(user: User) -> bool:
    """Проверяет доступны ли все сайты"""
    return not Site.objects.filter(user=user, status=False).exists()


def get_sites_with_checks(user: User) -> QuerySet:
    return Site.objects.annotate_newest_comment_text().filter(user=user).prefetch_related("checks")


def get_sites_by_hour(site: Site):
    """Получает список сайтов с процентом успешных запросов в час"""
    return (
        SiteCheck.objects.filter(site=site)
        .annotate(hour=TruncHour("created_at"))
        .values("hour")
        .annotate(
            success_count=Count("status_code", filter=Q(status_code__gte=200, status_code__lt=300)),
            total_count=Count("status_code"),
        )
    )


def register_user(email, password):
    """Регистрация нового пользователя"""
    user = User(email=email, role=Role.user)
    user.set_password(password)
    user.save()
    return user


def check_all_sites():
    site_checks = []
    sites = []

    for site in Site.objects.all():
        error_content = None
        try:
            response = requests.get(site.url)
            if not response.ok:
                error_content = getattr(response, "content", str(e))
        except requests.exceptions.ConnectionError as e:
            response = e.response
            error_content = str(e)

        check = SiteCheck(
            site=site,
            last_url=getattr(response, "url", site.url),
            status_code=getattr(response, "status_code", 0),
            error_response_content=error_content,
        )
        site.status = check.is_success

        site_checks.append(check)
        sites.append(site)

    SiteCheck.objects.bulk_create(site_checks)
    Site.objects.bulk_update(sites, ["status"])
    return sites
