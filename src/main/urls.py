"""webmonitoring URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path

from main.views import (
    main_page,
    site_page_via_site_name,
    site_page_pagination,
    SitePageDetailView,
    RedirectToMainView,
    SitesListView,
    auth_page,
    registration_page,
    profile_page,
    logout_page,
)

urlpatterns = [
    path("", main_page, name="main"),
    path("auth/", auth_page, name="auth"),
    path("registration/", registration_page, name="registration"),
    path("profile/", profile_page, name="profile"),
    path("logout/", logout_page, name="logout"),
    path("redirect_to_main/", RedirectToMainView.as_view()),
    path("sites/", SitesListView.as_view(), name="sites"),
    path("sites/<int:pk>/", SitePageDetailView.as_view(), name="site_page"),
    re_path(r"^sites/(?:page-(?P<number>\d+)/)?$", site_page_pagination, name="site_page_pagination"),
    path("sites/<str:site_name>/", site_page_via_site_name, name="site_page_via_site_name"),
]
