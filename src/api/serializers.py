from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from main.models import Site

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "email")


class SiteSerializer(serializers.ModelSerializer):
    domain = serializers.SerializerMethodField()
    address = serializers.CharField(source="url")

    @swagger_serializer_method(serializer_or_field=serializers.CharField(help_text="Домен сайта"))
    def get_domain(self, instance):
        return instance.url.split("://")[1]

    def validate_url(self, value):
        if len(value) < 13:
            raise ValidationError("URL слишком короткий")
        return value

    class Meta:
        model = Site
        fields = ("id", "url", "address", "status", "domain")
        read_only_fields = ("status",)


class SiteFullSerializer(SiteSerializer):
    user = UserSerializer()

    class Meta:
        model = Site
        fields = (*SiteSerializer.Meta.fields, "user")
