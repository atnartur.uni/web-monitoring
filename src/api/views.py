from drf_yasg.utils import swagger_auto_schema, no_body
from rest_framework import viewsets
from rest_framework.decorators import api_view, action
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.serializers import SiteSerializer, SiteFullSerializer
from api.tasks import check_sites
from main.models import Site


@api_view(["GET"])
def main_api_view(request):
    """Method for checking api"""
    check_sites.delay()
    return Response({"status": "ok"})


class SiteChangeOnlyForOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return obj.user_id == request.user.id


class SitesViewSet(viewsets.ModelViewSet):
    """Сайты"""

    pagination_class = LimitOffsetPagination
    permission_classes = [SiteChangeOnlyForOwnerPermission]

    def get_serializer_class(self):
        if self.action == "retrieve":
            return SiteFullSerializer
        return SiteSerializer

    def get_queryset(self):
        user = self.request.user
        qs = Site.objects.select_related("user")
        if user.is_authenticated:
            qs = qs.filter(user=user)
        return qs

    def list(self, request, *args, **kwargs):
        """Выводит список сайтов"""
        return super(SitesViewSet, self).list(request, *args, **kwargs)

    @swagger_auto_schema(
        method="post",
        request_body=no_body,
        operation_summary="Site check",
        responses={200: "Site checked"},
    )
    @action(["POST"], detail=True)
    def check(self, request, *args, **kwargs):
        """Проверка одного сайта"""
        obj = self.get_object()
        print(obj)
        return Response({"status": "ok", "id": obj.id})
