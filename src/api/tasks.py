import logging

from main.services import check_all_sites
from webmonitoring.celery import app

logger = logging.getLogger(__name__)


@app.task(queue="default")
def check_sites():
    checked_sites = check_all_sites()
    logger.info(f"checked {len(checked_sites)} sites")
