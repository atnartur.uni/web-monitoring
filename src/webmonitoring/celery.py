import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webmonitoring.settings")

app = Celery("webmonitoring")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks(packages=["api"])
